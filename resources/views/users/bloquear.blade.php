@extends('admin::layouts.anonymous-master')

@section('page_title')
    {{ __('admin::app.sessions.register.title') }}
@stop

@section('content')
    <div class="panel">
        <div class="panel-body">

            <div class="form-container">
                <h1>Seu período de teste terminou. Assine um plano conosco.</h1>

                <script type="text/javascript">
                    function importHotmart(){ 
                         var imported = document.createElement('script'); 
                         imported.src = 'https://static.hotmart.com/checkout/widget.min.js'; 
                         document.head.appendChild(imported); 
                        var link = document.createElement('link'); 
                        link.rel = 'stylesheet'; 
                        link.type = 'text/css'; 
                        link.href = 'https://static.hotmart.com/css/hotmart-fb.min.css'; 
                        document.head.appendChild(link);	} 
                     importHotmart(); 
                 </script> 
                 
                 <a onclick="return false;" href="https://pay.hotmart.com/Y74664094V?checkoutMode=2" class="hotmart-fb hotmart__button-checkout"><img src='https://static.hotmart.com/img/btn-buy-green.png'></a> 
                 
                 <div class="button-group">
                        <a href="https://www.heupy.com">
                            <button type="submit" class="btn btn-xl btn-primary">
                                Entre em contato
                            </button>
                        </a>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop

@push('scripts')
    <script>
        $(() => {
            $('input').keyup(({target}) => {
                if ($(target).parent('.has-error').length) {
                    $(target).parent('.has-error').addClass('hide-error');
                }
            });

            $('button').click(() => {
                $('.hide-error').removeClass('hide-error');
            });
        });
    </script>

<script type="text/javascript">
    function importHotmart(){ 
         var imported = document.createElement('script'); 
         imported.src = 'https://static.hotmart.com/checkout/widget.min.js'; 
         document.head.appendChild(imported); 
        var link = document.createElement('link'); 
        link.rel = 'stylesheet'; 
        link.type = 'text/css'; 
        link.href = 'https://static.hotmart.com/css/hotmart-fb.min.css'; 
        document.head.appendChild(link);	} 
     importHotmart(); 
 </script> 
@endpush
