@extends('admin::layouts.anonymous-master')

@section('page_title')
    {{ __('admin::app.sessions.register.title') }}
@stop

@section('content')
    <div class="panel">
        <div class="panel-body">

            <div class="form-container">
                <h1>{{ __('admin::app.sessions.register.welcome') }}</h1>

                <form method="POST" action="{{ route('user.register.store') }}" @submit.prevent="$root.onSubmit">
                    {!! view_render_event('admin.sessions.register.form_controls.before') !!}

                    @csrf

                    <div class="form-group" :class="[errors.has('name') ? 'has-error' : '']">
                        <label class="required">
                            {{ __('admin::app.sessions.register.name') }}
                        </label>

                        <input
                            type="text"
                            name="name"
                            class="control"
                            value="{{ old('name') }}"
                            placeholder="{{ __('admin::app.sessions.register.name') }}"
                            v-validate="'required'"
                            data-vv-as="{{ __('admin::app.sessions.register.name') }}"
                        />

                        <span class="control-error" v-if="errors.has('name')">
                            @{{ errors.first('name') }}
                        </span>
                    </div>

                    <div class="form-group" :class="[errors.has('email') ? 'has-error' : '']">
                        <label class="required">
                            {{ __('admin::app.sessions.register.email') }}
                        </label>

                        <input
                            type="email"
                            name="email"
                            class="control"
                            value="{{ old('email') }}"
                            placeholder="{{ __('admin::app.sessions.register.email') }}"
                            v-validate="'required|email'"
                            data-vv-as="{{ __('admin::app.sessions.register.email') }}"
                        />

                        <span class="control-error" v-if="errors.has('email')">
                            @{{ errors.first('email') }}
                        </span>
                    </div>

                    <div class="form-group" :class="[errors.has('phone') ? 'has-error' : '']">
                        <label class="required">
                            {{ __('admin::app.sessions.register.phone') }}
                        </label>

                        <input
                            type="tel"
                            name="phone"
                            class="control"
                            value="{{ old('phone') }}"
                            placeholder="{{ __('admin::app.sessions.register.phone') }}"
                            v-validate="'required'"
                            data-vv-as="{{ __('admin::app.sessions.register.phone') }}"
                        />

                        <span class="control-error" v-if="errors.has('phone')">
                            @{{ errors.first('phone') }}
                        </span>
                    </div>

                    <div class="form-group" :class="[errors.has('password') ? 'has-error' : '']">
                        <label class="required">
                            {{ __('admin::app.sessions.register.password') }}
                        </label>

                        <input
                            type="password"
                            name="password"
                            class="control"
                            ref="password"
                            placeholder="{{ __('admin::app.sessions.register.password') }}"
                            v-validate="'required|min:6'"
                            data-vv-as="{{ __('admin::app.sessions.register.password') }}"
                        />

                        <span class="control-error" v-if="errors.has('password')">
                            @{{ errors.first('password') }}
                        </span>
                    </div>

                    <div class="form-group" :class="[errors.has('confirm_password') ? 'has-error' : '']">
                        <label class="required">
                            {{ __('admin::app.sessions.register.confirm_password') }}
                        </label>

                        <input
                            type="password"
                            class="control"
                            name="confirm_password"
                            placeholder="{{ __('admin::app.sessions.register.confirm_password') }}"
                            v-validate="'required|confirmed:password'"
                            data-vv-as="{{ __('admin::app.sessions.register.confirm_password') }}"
                        />

                        <span class="control-error" v-if="errors.has('confirm_password')">
                            @{{ errors.first('confirm_password') }}
                        </span>
                    </div>

                    {!! view_render_event('admin.sessions.register.form_controls.after') !!}

                    <div class="button-group">
                        {!! view_render_event('admin.sessions.register.form_buttons.before') !!}

                        <button type="submit" class="btn btn-xl btn-primary">
                            {{ __('admin::app.sessions.register.register') }}
                        </button>

                        {!! view_render_event('admin.sessions.register.form_buttons.after') !!}
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop

@push('scripts')
    <script>
        $(() => {
            $('input').keyup(({target}) => {
                if ($(target).parent('.has-error').length) {
                    $(target).parent('.has-error').addClass('hide-error');
                }
            });

            $('button').click(() => {
                $('.hide-error').removeClass('hide-error');
            });
        });
    </script>
@endpush
