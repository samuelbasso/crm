<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
    </head>
    <body class="antialiased">
        <div class="table-responsive">
            <table border="1px">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>name</th>
                        <th>status</th>
                        <th>created_at</th>
                        <th>expired_at</th>
                        <th>phone</th>
                        <th>email</th>                        
                    </tr>
                </thead>
                <tbody>
                    @foreach ($tenants as $tenant)
                        <tr>
                            <th scope="row">{{ $tenant->id }}</th>
                            <td>{{ $tenant->name }}</td>
                            <td>{{ $tenant->status }}</td>
                            <td>{{ $tenant->created_at }}</td>
                            <td>{{ $tenant->expired_at }}</td>
                            <td>{{ $tenant->phone }}</td>
                            <td>{{ $tenant->email }}</td>                            
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </body>
</html>
