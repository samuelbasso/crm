echo "Instalar dependências"
sleep 1
docker-compose exec app composer install
docker-compose run node npm install
docker-compose run node npm --prefix ./packages/Webkul/Admin/ install
docker-compose run node npm --prefix ./packages/Webkul/UI/ install

echo "Atualizar dependências"
sleep 1
docker-compose exec app composer update
docker-compose run node npm update
docker-compose run node npm run prod
docker-compose run node npm --prefix ./packages/Webkul/Admin/ run prod
docker-compose run node npm --prefix ./packages/Webkul/UI/ run prod