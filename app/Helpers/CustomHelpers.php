<?php

use Illuminate\Support\Arr;

if (!function_exists('checkTenantId')) {
    function checkTenantId()
    {
        return session()->has('tenant_id') && !is_null(session('tenant_id'));
    }
}

if (!function_exists('addWhereTenantId')) {
    function addWhereTenantId($queryBuilder, $alias = null)
    {
        if (checkTenantId()) {
            if ($alias) {
                $queryBuilder = $queryBuilder->where($alias . '.tenant_id', session('tenant_id'));
            } else {
                $queryBuilder = $queryBuilder->where('tenant_id', session('tenant_id'));
            }
        }
        return $queryBuilder;
    }
}

if (!function_exists('addCreateTenantId')) {
    function addCreateTenantId(array $data)
    {
        if (checkTenantId()) {
            $data = Arr::add($data, 'tenant_id', session('tenant_id'));
        }
        return $data;
    }
}
