<?php

namespace App\Enums;

final class StatuTenantEnum 
{
    const ACTIVE = 1;
    const INACTIVE = 0;
}
