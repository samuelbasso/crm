<?php

namespace App\Http\Controllers;

use App\Models\Tenant;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Webkul\Lead\Models\Pipeline;
use Webkul\Lead\Models\Source;
use Webkul\Lead\Models\Stage;
use Webkul\Lead\Models\Type;

class UserController extends Controller
{
    
    public function create()
    {
        return view('users.create');
    }

    public function bloquear()
    {
        return view('users.bloquear');
    }

    public function getFreeTrialDaysLeftAttribute()
    {
        // Future field that will be implemented after payments
        // if ($this->plan_until) {
        //     return 0;
        // }

        // return now()->diffInDays($this->trial_until, false);
    }

    public function store(Request $request)
    {
        if (checkTenantId()) {
            session()->forget('tenant_id');
        }

        $this->validate(request(), [
            'email'            => 'required|email|unique:users,email',
        ]);
        
        $tenant = new Tenant();
        $tenant->name = $request['name'];
        $tenant->phone = $request['phone'];
        $tenant->email = $request['email'];
        $tenant->plan = 'demo';
        $tenant->expired_at = Carbon::now()->addDays(config('app.free_trial_days'));
        $tenant->save();

        $source = new Source();
        $source->name = 'Prospecção Ativa';
        $source->tenant_id = $tenant->id;
        $source->save();

        $type = new Type();
        $type->name = 'Venda';
        $type->tenant_id = $tenant->id;
        $type->save();

        $lead_pipeline = new Pipeline();
        $lead_pipeline->name = 'Padrão';
        $lead_pipeline->is_default = 1;
        $lead_pipeline->created_at = Carbon::now();
        $lead_pipeline->updated_at = Carbon::now();
        $lead_pipeline->tenant_id  = $tenant->id;
        $lead_pipeline->save();

        $lead_pipeline_stages = new Stage();
        $lead_pipeline_stages->code = 'new';
        $lead_pipeline_stages->name = 'Sem Contato';
        $lead_pipeline_stages->probability = 100;
        $lead_pipeline_stages->sort_order = 1;
        $lead_pipeline_stages->lead_pipeline_id = $lead_pipeline->id;
        $lead_pipeline_stages->save();

        $lead_pipeline_stages = new Stage();
        $lead_pipeline_stages->code = 'contato';
        $lead_pipeline_stages->name = 'Contato Feito';
        $lead_pipeline_stages->probability = 100;
        $lead_pipeline_stages->sort_order = 2;
        $lead_pipeline_stages->lead_pipeline_id = $lead_pipeline->id;
        $lead_pipeline_stages->save();

        $lead_pipeline_stages = new Stage();
        $lead_pipeline_stages->code = 'proposta';
        $lead_pipeline_stages->name = 'Proposta';
        $lead_pipeline_stages->probability = 100;
        $lead_pipeline_stages->sort_order = 3;
        $lead_pipeline_stages->lead_pipeline_id = $lead_pipeline->id;
        $lead_pipeline_stages->save();
        
        $lead_pipeline_stages = new Stage();
        $lead_pipeline_stages->code = 'negociacao';
        $lead_pipeline_stages->name = 'Negociação';
        $lead_pipeline_stages->probability = 100;
        $lead_pipeline_stages->sort_order = 4;
        $lead_pipeline_stages->lead_pipeline_id = $lead_pipeline->id;
        $lead_pipeline_stages->save();
        
        $lead_pipeline_stages = new Stage();
        $lead_pipeline_stages->code = 'won';
        $lead_pipeline_stages->name = 'Ganho';
        $lead_pipeline_stages->probability = 100;
        $lead_pipeline_stages->sort_order = 5;
        $lead_pipeline_stages->lead_pipeline_id = $lead_pipeline->id;
        $lead_pipeline_stages->save();

        $lead_pipeline_stages = new Stage();
        $lead_pipeline_stages->code = 'lost';
        $lead_pipeline_stages->name = 'Perdido';
        $lead_pipeline_stages->probability = 100;
        $lead_pipeline_stages->sort_order = 6;
        $lead_pipeline_stages->lead_pipeline_id = $lead_pipeline->id;
        $lead_pipeline_stages->save();

        $user = User::create([
            'name' => $request['name'],
            'email' => $request['email'],
            'password' => Hash::make($request['password']),
            'role_id' => 1,
            'status' => 1,
            'view_permission' => 'global',
            'tenant_id' => $tenant->id,
        ]);
        Auth::login($user);
        return redirect('/');
    }
}
