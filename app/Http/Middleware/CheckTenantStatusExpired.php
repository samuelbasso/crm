<?php

namespace App\Http\Middleware;

use App\Enums\StatuTenantEnum;
use App\Models\Tenant;
use Carbon\Carbon;
use Closure;

class CheckTenantStatusExpired
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle($request, Closure $next)
    {
        if (session('tenant_id') != null) {
            $tenant = Tenant::find(session('tenant_id'));
            $planoExpirado = Carbon::now()->gte($tenant->expired_at);
            if ($tenant->plan == 'demo' && $planoExpirado) {
                $tenant->status = StatuTenantEnum::INACTIVE;
                $tenant->save();
            }
        

            if ($tenant->status == StatuTenantEnum::INACTIVE) {
                return redirect('/bloquear');
            }
        }
        return $next($request);
    }
}
