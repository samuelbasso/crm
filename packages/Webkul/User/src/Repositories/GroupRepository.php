<?php

namespace Webkul\User\Repositories;

use Webkul\Core\Eloquent\Repository;

class GroupRepository extends Repository
{
    /**
     * Specify Model class name
     *
     * @return mixed
     */
    function model()
    {
        return 'Webkul\User\Contracts\Group';
    }

    public function create(array $data)
    {
        return parent::create(addCreateTenantId($data));
    }
}