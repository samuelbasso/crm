<?php

namespace Webkul\User\Database\Seeders;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;

class TenantSeeder extends Seeder
{
    public function run()
    {
        DB::table('tenants')->delete();

        DB::table('tenants')->insert([
            'id'              => 1,
            'name'            => 'Tenant',
        ]);
    }
}