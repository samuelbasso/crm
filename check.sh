echo "Code Quality"
sleep 1
docker-compose exec app ./vendor/bin/phpcbf
docker-compose exec app ./vendor/bin/phpcs -p
docker-compose exec app php artisan insights

echo "Tests"
sleep 1
docker-compose exec app php artisan test --env=testing
