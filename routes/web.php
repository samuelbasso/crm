<?php

use App\Http\Controllers\TenantController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/register', [UserController::class, 'create'])->name('user.register.create');
Route::post('/register', [UserController::class, 'store'])->name('user.register.store');
Route::get('/bloquear', [UserController::class, 'bloquear'])->name('user.bloquear.create')
    ->withoutMiddleware('CheckTenantStatusExpired');

Route::get('/admin/tenant', [TenantController::class, 'index'])->name('tenant.index');


Route::get('/', function () {
    return view('welcome');
});
